/**
 * SnippetsController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */


var http = require("http");
var https = require("https");

function all_items_present(str, items) {
 var i;
 var len=items.length;
 var found=true;

    for(i=0;i<len;i++) {
        if(str.search(items[i])==-1) {
            found=false;
            break;
        }
    }

    return found;
}


module.exports = {

	search: function(req, res) {

		// If req is empty then instrucctions

		// if req is number then retrieve specific snippet / s

		// else search for snippets available in bitbucket 

//		console.dir(req.body.item.message);
//		console.log(req.body.item.message.message.split(' ').splice(1));


		https.get("https://api.bitbucket.org/2.0/snippets/supportsnippets", function(res) {

			res.setEncoding('utf8');
			var output = '';

			var list = '';


	        res.on('data', function (chunk) {
	            output += chunk;
	        });

	        res.on('end', function() {
	            var obj = JSON.parse(output);
	            //console.log(obj);

	            var params = req.body.item.message.message.toLowerCase().split(' ').splice(1) ;
	            var hcmessage = "";

	            if (params.length === 0){
	            	hcmessage = "<b>Hipchat Snippets!</b><br> Find a snippet -> /snip < your_search > <br> <a href=\"https://bitbucket.org/snippets/new?owner=supportsnippets\">Create a new snippet in Bitbucket!</a>";

	            } else {

		            var index = 0;
					for( myKey in obj.values) {
						if ( all_items_present( obj.values[myKey].title.toLowerCase() , params)){

					   		hcmessage += ++index +" . <a href=\""+obj.values[myKey].links.html.href +"\">"+obj.values[myKey].title+"</a><br>" ;
						}
					}

					if (index == 0){
						hcmessage = "Sorry, no snippets were returned for that search.<br><a href=\"https://bitbucket.org/snippets/new?owner=supportsnippets\">Create a new one!</a>";
					}
				}


				var options = {
					hostname: 'api.hipchat.com',
					port: 443,
					path: '/v2/room/1860177/notification?auth_token=VxqWsxFQsorYwxLL8UIQUnrN98CnpCqAoMKpDDmr',
					method: 'POST',
					headers: {
					    'Content-Type': 'application/json'
					}
				}
				var post_data = JSON.stringify(
					{	
						"color": "purple",
					 	"message": hcmessage,
					 	"notify": false,
					 	"message_format": "html"
					 }
				);

				var post_req = https.request(options, function(res) {

					res.on('end', function() {
						console.log("end of req" );
					});
				}).on('error', function(e) {
					console.log("Got error sending message back to hipchat: " + e.message);
				});

				post_req.write(post_data);
				post_req.end();
				console.log("Snippet sent to Hipchat" );
	        });

		}).on('error', function(e) {
			console.log("Got error: " + e.message);
		});
        
        res.jsonp(200, {})

    },
    
  


  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to SnippetsController)
   */
  _config: {}

  
};
